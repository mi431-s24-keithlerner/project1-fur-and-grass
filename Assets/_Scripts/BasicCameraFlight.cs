using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicCameraFlight : MonoBehaviour
{
    public KeyCode doubleSpeed = KeyCode.Space;
    
    public float speed = 1;
    public float turn = 1;
    
    private Vector3 deltaPos = Vector3.zero;
    
    void Update()
    {
        
        transform.Rotate(  transform.up, Input.GetAxis("Horizontal") * 
            (Input.GetKey(doubleSpeed) ? 2 : 1) * turn * Time.deltaTime  );

        deltaPos = Vector3.zero;
            
        deltaPos += transform.forward * 
                    (Input.GetAxis("Vertical") * (Input.GetKey(doubleSpeed) ? 2 : 1) * speed * Time.fixedDeltaTime);
        
        if (Input.GetKey(KeyCode.C))
            deltaPos += transform.right * ((Input.GetKey(doubleSpeed) ? 2 : 1) * speed * Time.fixedDeltaTime);
        
        if (Input.GetKey(KeyCode.Z))
            deltaPos -= transform.right * ((Input.GetKey(doubleSpeed) ? 2 : 1) * speed * Time.fixedDeltaTime);

        if (Input.GetKey(KeyCode.E))
            deltaPos += Vector3.up * ((Input.GetKey(doubleSpeed) ? 2 : 1) * speed * Time.fixedDeltaTime);
        
        if (Input.GetKey(KeyCode.Q))
            deltaPos -= Vector3.up * ((Input.GetKey(doubleSpeed) ? 2 : 1) * speed * Time.fixedDeltaTime);
    }
    
    void FixedUpdate()
    {
        transform.position += deltaPos;
    }
}
